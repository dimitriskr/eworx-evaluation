/**
 * @file
 * Article View modifications behaviors.
 */

(function (Drupal) {

  /**
   * Behavior description.
   */
  Drupal.behaviors.articleView = {
    attach: function (context, settings) {

      const articles = document.getElementsByClassName('my-view-item');
      for (let i = 0; i < articles.length; i++) {
        articles[i].classList.add('view-item-processed');
      }
    }
  };

}(Drupal));

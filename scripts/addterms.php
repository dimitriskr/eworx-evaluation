<?php

/** @var \Drupal\taxonomy\TermStorageInterface $term_storage */
$term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');

$terms_to_add = [
  'General',
  'Politics',
  'Social',
];

$parent_vocabulary_id = 'article_categories';

foreach ($terms_to_add as $term) {
  /** @var \Drupal\taxonomy\Entity\Term $term_entity */
  $term_entity = $term_storage->create([
    'name' => $term,
    'vid' => $parent_vocabulary_id,
  ]);
  $term_entity->save();
  echo $term . ' created' . PHP_EOL;
}

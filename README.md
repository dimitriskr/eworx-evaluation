# How to install the project

## Requirements
- Apache/Nginx webserver
- MariaDB
- PHP 7.4
- Composer 2

## Steps
1) Download/clone the project
2) Enter the project folder via a terminal
3) Run `composer install` to install all dependencies of the project
4) Copy `web/sites/default/default.settings.php` to `web/sites/default/settings.php` and append the text located in `assets/settings-php-additions`. Make sure to edit the DB credentials and DB host/port per your setup.
5) Navigate to the site via a browser, per your setup configuration and follow the steps.
6) On step "Installation profile" step, choose "Use existing configuration".
   1) You can skip the last two parts by running `vendor/bin/drush si --existing-config -y`
7) After the site is properly configured and installed, create some content to test the features by going to `/node/add/article`
8) Run `vendor/bin/drush scr scripts/addterms.php` to add the terms to "Article Categories" taxonomy
9) Enjoy!
### In case you have DDEV installed

1) Run `ddev start`
2) Run `ddev composer install` to install all site dependencies
3) Add the text of `assets/settings-php-additions` to the bottom of the generated settings.php
4) Install the site as described above, steps 5-6 (In order to visit the site, run `ddev launch`)
5) After the installation is complete, visit https://eworx-evaluation.ddev.site to browse with the credentials you inserted or from the terminal output
6) After the site is properly configured and installed, create some content to test the features by going to [Add article](https://eworx-evaluation.ddev.site/node/add/article)
7) Run `ddev drush scr scripts/addterms.php` to add the terms to "Article Categories" taxonomy
8) Enjoy!
